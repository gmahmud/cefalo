### Environment Dependencies
* Node
* Gulp

### Running the project ###

Run the following command from the project root directory - ```gulp```

### Note ###
* Development assets are in the ```src``` directory
* Production assets are in the ```dest``` directory
* No CSS (frontend design framewrok) and JavaScript dependency
* image optimization through gulp-imagemin

### Page-1 ( [list.html](http://209.59.190.118/~ganmahmu/projects/cefalo/list.html "View it online") ) ###
All the links in this page are dummy links except “Make your own list”. By clicking on this link, you should be redirected to the Page-2 (login.html), assuming you
are not logged into the system. 

### Page-2 ( [login.html](http://209.59.190.118/~ganmahmu/projects/cefalo/login.html "View it online") ) ###
* In the desktop and tablet version, “Login with Facebook” button should be vertically center aligned, on the right side of the text “LET’S MAKE YOUR LIST”, but on the mobile version, the login button should be placed at the bottom of the text.
* By clicking on the “Login with Facebook” button, you should be redirected to Page-3 (createlist.html) 

### Page-3 ( [createlist.html](http://209.59.190.118/~ganmahmu/projects/cefalo/createlist.html "View it online") ) ###
This is the page where you get redirected after you have logged in using Facebook.

* In the dropdown "SELECT YOUR TOP3 LIST" you can choose between: (1) My three best leaders, (2) Guilty of my career, (3) The most creative I know,
(4) Top 3 on sales, (5) Top 3 designers, (6) Top 3 copyrighters, (7) Top 3 photographers and (8) The funniest people I know.

* When clicking on the circles with the plus sign you have to choose your Facebook-connection to include in the list. Let’s keep this circular button dummy
for simplicity.

* By clicking on the “Preview my list”, you should be redirected to Page-4 (mylist.html) 

### Page-4 ( [mylist.html](http://209.59.190.118/~ganmahmu/projects/cefalo/mylist.html "View it online") ) ###
* By clicking on the “No, edit my list” button, you should be redirected to Page-3 (createlist.html)
* By clicking on the “Yes, Publish and share my list >” button, you should be redirected to Page-1 (list.html)
* Each of the leaders information block (avatar, title and summary) should be clickable and pointing to external dummy link. 